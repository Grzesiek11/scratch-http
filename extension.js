class HttpExtension {
    constructor() {
        this.requests = {};
        this.sockets = {};
        this.nextHandle = 0;
    }

    getInfo() {
        return {
            id: "http",
            name: "HTTP",
            blocks: [
                {
                    opcode: "requestNew",
                    text: "new [METHOD] request to URL [URL]",
                    blockType: Scratch.BlockType.REPORTER,
                    arguments: {
                        METHOD: {
                            type: Scratch.ArgumentType.STRING,
                            menu: "requestMethods",
                            defaultValue: "GET",
                        },
                        URL: {
                            type: Scratch.ArgumentType.STRING,
                            defaultValue: " ",
                        },
                    },
                },
                {
                    opcode: "requestMake",
                    text: "make request [REQUEST]",
                    blockType: Scratch.BlockType.COMMAND,
                    arguments: {
                        REQUEST: {
                            type: Scratch.ArgumentType.NUMBER,
                            defaultValue: " ",
                        },
                    },
                },
                {
                    opcode: "requestDelete",
                    text: "delete request [REQUEST]",
                    blockType: Scratch.BlockType.COMMAND,
                    arguments: {
                        REQUEST: {
                            type: Scratch.ArgumentType.NUMBER,
                            defaultValue: " ",
                        },
                    },
                },
                {
                    opcode: "requestSetBody",
                    text: "set request [REQUEST] body to [BODY]",
                    blockType: Scratch.BlockType.COMMAND,
                    arguments: {
                        REQUEST: {
                            type: Scratch.ArgumentType.NUMBER,
                            defaultValue: " ",
                        },
                        BODY: {
                            type: Scratch.ArgumentType.STRING,
                            defaultValue: " ",
                        },
                    },
                },
                {
                    opcode: "requestSetHeader",
                    text: "set request [REQUEST] header [HEADER] to [VALUE]",
                    blockType: Scratch.BlockType.COMMAND,
                    arguments: {
                        REQUEST: {
                            type: Scratch.ArgumentType.NUMBER,
                            defaultValue: " ",
                        },
                        HEADER: {
                            type: Scratch.ArgumentType.STRING,
                            defaultValue: " ",
                        },
                        VALUE: {
                            type: Scratch.ArgumentType.STRING,
                            defaultValue: " ",
                        },
                    },
                },
                {
                    opcode: "requestResponseBody",
                    text: "response [REQUEST] body",
                    blockType: Scratch.BlockType.REPORTER,
                    arguments: {
                        REQUEST: {
                            type: Scratch.ArgumentType.NUMBER,
                            defaultValue: " ",
                        },
                    },
                },
                {
                    opcode: "requestResponseHeader",
                    text: "response [REQUEST] header [HEADER]",
                    blockType: Scratch.BlockType.REPORTER,
                    arguments: {
                        REQUEST: {
                            type: Scratch.ArgumentType.NUMBER,
                            defaultValue: " ",
                        },
                        HEADER: {
                            type: Scratch.ArgumentType.STRING,
                            defaultValue: " ",
                        },
                    },
                },
                {
                    opcode: "requestResponseStatus",
                    text: "response [REQUEST] status",
                    blockType: Scratch.BlockType.REPORTER,
                    arguments: {
                        REQUEST: {
                            type: Scratch.ArgumentType.NUMBER,
                            defaultValue: " ",
                        },
                    },
                },
                {
                    opcode: "websocketConnect",
                    text: "connect to websocket URL [URL]",
                    blockType: Scratch.BlockType.REPORTER,
                    arguments: {
                        URL: {
                            type: Scratch.ArgumentType.STRING,
                            defaultValue: " ",
                        },
                    },
                },
                {
                    opcode: "websocketDisconnect",
                    text: "disconnect from websocket [SOCKET]",
                    blockType: Scratch.BlockType.COMMAND,
                    arguments: {
                        SOCKET: {
                            type: Scratch.ArgumentType.NUMBER,
                            defaultValue: " ",
                        },
                    },
                },
                {
                    opcode: "websocketStatus",
                    text: "websocket [SOCKET] status",
                    blockType: Scratch.BlockType.REPORTER,
                    arguments: {
                        SOCKET: {
                            type: Scratch.ArgumentType.NUMBER,
                            defaultValue: " ",
                        },
                    },
                },
                {
                    opcode: "websocketSend",
                    text: "send [MESSAGE] to websocket [SOCKET]",
                    blockType: Scratch.BlockType.COMMAND,
                    arguments: {
                        MESSAGE: {
                            type: Scratch.ArgumentType.STRING,
                            defaultValue: " ",
                        },
                        SOCKET: {
                            type: Scratch.ArgumentType.NUMBER,
                            defaultValue: " ",
                        },
                    },
                },
                {
                    opcode: "websocketPending",
                    text: "pending messages in websocket [SOCKET]",
                    blockType: Scratch.BlockType.BOOLEAN,
                    arguments: {
                        SOCKET: {
                            type: Scratch.ArgumentType.NUMBER,
                            defaultValue: " ",
                        },
                    },
                },
                {
                    opcode: "websocketReceive",
                    text: "receive from websocket [SOCKET]",
                    blockType: Scratch.BlockType.REPORTER,
                    arguments: {
                        SOCKET: {
                            type: Scratch.ArgumentType.NUMBER,
                            defaultValue: " ",
                        },
                    },
                },
                {
                    opcode: "websocketDelete",
                    text: "delete websocket [SOCKET]",
                    blockType: Scratch.BlockType.COMMAND,
                    arguments: {
                        SOCKET: {
                            type: Scratch.ArgumentType.NUMBER,
                            defaultValue: " ",
                        },
                    },
                },
            ],
            menus: {
                requestMethods: {
                    acceptReporters: true,
                    items: [
                        "GET",
                        "HEAD",
                        "POST",
                        "PUT",
                        "DELETE",
                        "CONNECT",
                        "OPTIONS",
                        "TRACE",
                        "PATCH",
                    ],
                },
            },
        };
    }

    requestNew(args) {
        const handle = this.nextHandle++;
        this.requests[handle] = {
            url: args.URL,
            method: args.METHOD,
            headers: new Headers(),
        };
        return handle;
    }

    async requestMake(args) {
        let request = this.requests[args.REQUEST];

        const response = await fetch(request.url, {
            method: request.method,
            body: request.body,
            headers: request.headers,
        });

        request.responseStatus = response.status;
        request.responseHeaders = response.headers;
        request.responseBody = await response.text();
    }

    requestDelete(args) {
        delete this.requests[args.REQUEST];
    }

    requestSetBody(args) {
        this.requests[args.REQUEST].body = args.BODY;
    }

    requestSetHeader(args) {
        this.requests[args.REQUEST].headers.set(args.HEADER, args.VALUE);
    }

    requestResponseBody(args) {
        return this.requests[args.REQUEST].responseBody;
    }

    requestResponseHeader(args) {
        return this.requests[args.REQUEST].responseHeaders.get(args.HEADER);
    }

    requestResponseStatus(args) {
        return this.requests[args.REQUEST].responseStatus;
    }

    websocketConnect(args) {
        const received = [];
        const socket = new WebSocket(args.URL);
        socket.addEventListener("message", (e) => {
            received.push(e.data);
        });

        const handle = this.nextHandle++;
        this.sockets[handle] = {
            socket: socket,
            received: received,
        };
        return handle;
    }

    websocketDisconnect(args) {
        this.sockets[args.SOCKET].socket.close();
    }

    websocketStatus(args) {
        return {
            0: "connecting",
            1: "open",
            2: "closing",
            3: "closed",
        }[this.sockets[args.SOCKET].socket.readyState];
    }

    websocketSend(args) {
        this.sockets[args.SOCKET].socket.send(args.MESSAGE);
    }

    websocketPending(args) {
        return this.sockets[args.SOCKET].received.length != 0;
    }

    websocketReceive(args) {
        return this.sockets[args.SOCKET].received.shift();
    }

    websocketDelete(args) {
        delete this.sockets[args.SOCKET];
    }
}

Scratch.extensions.register(new HttpExtension());
